"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppHttpExceptionFilter = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const response_1 = require("../response");
const exception_1 = require("../exception");
const codex_data_model_1 = require("@cryptexlabs/codex-data-model");
const locales_1 = require("../locales/locales");
const enum_1 = require("../locales/enum");
const config_1 = require("../config");
let AppHttpExceptionFilter = class AppHttpExceptionFilter extends core_1.BaseExceptionFilter {
    constructor(fallbackLocale, fallbackLogger, config, applicationRef) {
        super(applicationRef);
        this.fallbackLocale = fallbackLocale;
        this.fallbackLogger = fallbackLogger;
        this.config = config;
    }
    catch(exception, host) {
        const ctx = host.switchToHttp();
        const request = ctx.getRequest();
        const response = ctx.getResponse();
        const logger = exception instanceof exception_1.FriendlyHttpException
            ? exception.context.logger
            : this.fallbackLogger;
        logger.error({
            request: {
                headers: request.headers,
                body: request.body,
                url: request.url,
                method: request.method,
            },
            exception,
        });
        if (!(exception instanceof common_1.HttpException) && exception.stack) {
            logger.error(exception.stack);
        }
        const status = exception.getStatus !== undefined &&
            typeof exception.getStatus === "function"
            ? exception.getStatus()
            : common_1.HttpStatus.INTERNAL_SERVER_ERROR;
        const locale = exception instanceof exception_1.FriendlyHttpException
            ? exception.context.locale
            : this.fallbackLocale;
        const correlationId = exception instanceof exception_1.FriendlyHttpException
            ? exception.context.correlationId
            : undefined;
        const started = exception instanceof exception_1.FriendlyHttpException
            ? exception.context.started
            : undefined;
        const developerText = this.getDeveloperText(exception, locale);
        const userMessage = exception instanceof exception_1.FriendlyHttpException ? exception.userMessage : "";
        const errorHttpResponse = new response_1.ErrorHttpResponse(status, locale, new response_1.ErrorMessage(locale, locales_1.i18nData, null, null, developerText, userMessage), exception.stack || null, this.config, correlationId, started);
        response.status(status).json(errorHttpResponse);
    }
    getDeveloperText(exception, locale) {
        let developerText = locales_1.i18nData.__({
            phrase: enum_1.LocalesEnum.UNKNOWN_ERROR,
            locale: locale.i18n,
        });
        if (exception) {
            if (exception.getStatus) {
                if (exception.response && exception.response.message) {
                    if (Array.isArray(exception.response.message)) {
                        developerText = exception.response.message.join(" + ");
                    }
                    else {
                        developerText = exception.response.message;
                    }
                }
                else if (exception.message.message) {
                    developerText = exception.message.message;
                }
                else {
                    developerText = exception.message;
                }
            }
        }
        return developerText;
    }
};
AppHttpExceptionFilter = __decorate([
    common_1.Catch(),
    common_1.Injectable(),
    __metadata("design:paramtypes", [codex_data_model_1.Locale, Object, config_1.DefaultConfig, Object])
], AppHttpExceptionFilter);
exports.AppHttpExceptionFilter = AppHttpExceptionFilter;
//# sourceMappingURL=app-http-exception-filter.js.map