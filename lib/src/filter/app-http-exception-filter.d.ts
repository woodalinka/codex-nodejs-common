import { ArgumentsHost, HttpServer, LoggerService } from "@nestjs/common";
import { BaseExceptionFilter } from "@nestjs/core";
import { Locale } from "@cryptexlabs/codex-data-model";
import { DefaultConfig } from "../config";
export declare class AppHttpExceptionFilter extends BaseExceptionFilter {
    private readonly fallbackLocale;
    private readonly fallbackLogger;
    private readonly config;
    constructor(fallbackLocale: Locale, fallbackLogger: LoggerService, config: DefaultConfig, applicationRef?: HttpServer);
    catch(exception: any, host: ArgumentsHost): void;
    getDeveloperText(exception: any, locale: any): string;
}
