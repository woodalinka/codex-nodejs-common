export interface HostConfigInterface {
    host: string;
    port: number;
}
