import { ArgumentMetadata, PipeTransform } from "@nestjs/common";
export declare class CountryCodePipe implements PipeTransform {
    transform(value: any, metadata: ArgumentMetadata): any;
}
