"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ElasticsearchPinger = void 0;
const common_1 = require("@nestjs/common");
const logger_1 = require("../../logger");
let ElasticsearchPinger = class ElasticsearchPinger {
    constructor(elasticsearchService, logger) {
        this.elasticsearchService = elasticsearchService;
        this.logger = logger;
        this.intervalId = null;
        this._resetPinger();
    }
    _resetPinger() {
        if (this.intervalId) {
            this.logger.debug("ES Pinger already running");
            return;
        }
        this.ready = false;
        this.logger.debug("Starting new ES pinger");
        this.intervalId = setInterval(async () => {
            this.logger.info("Waiting for elasticsearch");
            this.ready = await this._isReady();
            if (this.ready) {
                clearInterval(this.intervalId);
                this.intervalId = null;
            }
            else {
                this.logger.debug("Elasticsearch not available", "debug");
            }
        }, 1000);
    }
    async waitForReady() {
        if (this.ready) {
            this._resetPinger();
        }
        return new Promise((resolve) => {
            const intervalId = setInterval(() => {
                if (this.ready) {
                    clearInterval(intervalId);
                    resolve(undefined);
                }
            }, 500);
        });
    }
    async _isReady() {
        return new Promise(async (resolve) => {
            try {
                const result = await this.elasticsearchService.ping();
                resolve(result);
            }
            catch (e) {
                this.logger.error(e.message, e.trace);
                resolve(false);
            }
        });
    }
};
ElasticsearchPinger = __decorate([
    common_1.Injectable(),
    __param(0, common_1.Inject("ELASTICSEARCH_SERVICE")),
    __param(1, common_1.Inject("LOGGER")),
    __metadata("design:paramtypes", [Object, logger_1.CustomLogger])
], ElasticsearchPinger);
exports.ElasticsearchPinger = ElasticsearchPinger;
//# sourceMappingURL=elasticsearch.pinger.js.map