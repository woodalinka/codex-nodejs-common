import { OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit } from "@nestjs/websockets";
import { LoggerService } from "@nestjs/common";
import { DefaultConfig } from "../../config";
import { AuthenticatorInterface, TopicAuthorizorInterface } from "../../auth";
import { SocketSubscribedClient } from "./socket-subscribed-client";
import { BroadcasterService } from "./broadcaster.service";
import { WebsocketConsumerService } from "../consumer";
import { ContextBuilder } from "../../context";
export declare class WebsocketGateway implements OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit {
    private readonly consumerService;
    private readonly config;
    private readonly authorizors;
    private readonly logger;
    private readonly broadcasterService;
    private readonly contextBuilder;
    private readonly authenticator;
    private server;
    private readonly subscribedTopics;
    constructor(consumerService: WebsocketConsumerService, config: DefaultConfig, authorizors: TopicAuthorizorInterface[], logger: LoggerService, broadcasterService: BroadcasterService, contextBuilder: ContextBuilder, authenticator: AuthenticatorInterface);
    afterInit(): Promise<void>;
    handleConnection(client: WebSocket): Promise<void>;
    _onMessage(event: MessageEvent, client: WebSocket, subscribedClient: SocketSubscribedClient): Promise<void>;
    private _subscribeTopics;
    handleDisconnect(client: any): void;
}
