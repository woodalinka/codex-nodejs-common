"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HttpResponseMeta = void 0;
const message_1 = require("../message");
class HttpResponseMeta extends message_1.MessageMeta {
    constructor(status, type, locale, config, correlationId, started) {
        super(type, locale, config, correlationId, started);
        this.status = status;
    }
    toJSON() {
        return {
            status: this.status,
            type: this.type,
            schemaVersion: this.schemaVersion,
            correlationId: this.correlationId,
            time: this.time,
            context: this.context,
            locale: this.locale,
            client: this.client,
        };
    }
}
exports.HttpResponseMeta = HttpResponseMeta;
//# sourceMappingURL=http-response-meta.js.map