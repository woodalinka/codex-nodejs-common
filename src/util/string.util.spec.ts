import { StringUtil } from "./string.util";

describe("StringUtil", () => {
  it("should be a regex string", () => {
    expect(
      StringUtil.isRegexString(
        "/reply\\.message\\.(app|sms|mms)\\.unassigned-interaction/"
      )
    ).toBe(true);
  });

  it("should not be a regex string", () => {
    expect(
      StringUtil.isRegexString(
        "/reply\\.message\\.(app|sms|mms)\\.unassigned-interaction"
      )
    ).toBe(false);
    expect(
      StringUtil.isRegexString(
        "reply\\.message\\.(app|sms|mms)\\.unassigned-interaction/"
      )
    ).toBe(false);
  });

  it("should match regex string", () => {
    expect(
      StringUtil.stringMatches(
        "/reply\\.message\\.(app|sms|mms)\\.unassigned-interaction/",
        "reply.message.mms.unassigned-interaction"
      )
    ).toBe(true);
    expect(
      StringUtil.stringMatches(
        "reply\\.message\\.(app|sms|mms)\\.unassigned-interaction/",
        "reply.message.asdf.unassigned-interaction"
      )
    ).toBe(false);
  });

  it("should md5 a string", () => {
    expect(StringUtil.insecureMd5("hello world")).toBe(
      "5eb63bbbe01eeed093cb22bb8f5acdc3"
    );
  });

  it("should sha1 a string", () => {
    expect(StringUtil.insecureSha1("hello world")).toBe(
      "2aae6c35c94fcfb415dbe95f408b9ce91ee846ed"
    );
  });

  it("should sha256 a string", () => {
    expect(StringUtil.sha256("hello world")).toBe(
      "b94d27b9934d3e08a52e52d7da7dabfac484efe37a5380ee9088f7ace2efcde9"
    );
  });

  it("should sha512 a string", () => {
    expect(StringUtil.sha512("hello world")).toBe(
      "309ecc489c12d6eb4cc40f50c902f2b4d0ed77ee511a7c7a9bcd3ca86d4cd86f989dd35bc5ff499670da34255b45b0cfd830e81f605dcf7dc5542e93ae9cd76f"
    );
  });
});
